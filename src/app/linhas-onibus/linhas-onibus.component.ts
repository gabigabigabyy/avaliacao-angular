import { Component, OnInit } from '@angular/core';
import {OnibusService} from '../onibus.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-linhas-onibus',
  templateUrl: './linhas-onibus.component.html',
  styleUrls: ['./linhas-onibus.component.css']
})
export class LinhasOnibusComponent implements OnInit {

  linhas: Array<any> | undefined;
  searchText: string="";

  constructor(private onibusService: OnibusService, private router:Router) { }

  ngOnInit() {
    this.listar();


  }

  listar(){
    this.onibusService.listar().subscribe (dados => this.linhas = dados);
  }
  teste(){this.router.navigateByUrl('/itinerario')}
}
