import { ItinerarioService } from './../itinerario.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-itinerario',
  templateUrl: './itinerario.component.html',
  styleUrls: ['./itinerario.component.css']
})
export class ItinerarioComponent implements OnInit {

  itinerario: Array<any> | undefined;
    searchText: string="";

  constructor(private itinerarioService: ItinerarioService) {

  }
  ngOnInit() {
    this.listar()
  }
  listar(){
    this.itinerarioService.listar().subscribe (dados => this.itinerario = dados);
  }
/* @Component({
  selector: 'app-itinerario',
  templateUrl: './itinerario.component.html',
  styleUrls: ['./itinerario.component.css']
  })
  export class ItinerarioComponent implements OnInit {

    linhas: Array<any> | undefined;
    searchText: string="";

    constructor(private itinerarioService: ItinerarioService) { }

    ngOnInit() {
      this.listar();
    }

    listar(){
      this.itinerarioService.listar().subscribe (dados => this.linhas = dados);
    }

    teste(){console.log("teste")};
  } */
}
