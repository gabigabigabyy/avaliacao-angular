import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ItinerarioService {

  linhasURL = 'http:/www.poatransporte.com.br/php/facades/process.php?a=il&p=5599'

  constructor(private http: HttpClient) { }

  listar(){
    return this.http.get<any[]>(`${this.linhasURL}`);
  }
 }
