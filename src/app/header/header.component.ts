import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  onibus(){this.router.navigateByUrl('/linhasdeonibus')}
  lotacao(){this.router.navigateByUrl('/linhasdelotacao')}
  inicio(){this.router.navigateByUrl('/inicio')}
  itinerario(){this.router.navigateByUrl('/itinerario')}

}
