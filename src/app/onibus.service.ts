import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable()
 
export class OnibusService {

  linhasURL = 'http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%25&t=o'
  
  constructor(private http: HttpClient) { }

    listar(){
      return this.http.get<any[]>(`${this.linhasURL}`);
    }
   }



  