import { ItinerarioComponent } from './itinerario/itinerario.component';
import { InicioComponent } from './inicio/inicio.component';
import { LinhasLotacaoComponent } from './linhas-lotacao/linhas-lotacao.component';
import { LinhasOnibusComponent} from './linhas-onibus/linhas-onibus.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';



const routes: Routes = [
{path:'linhasdeonibus',component:LinhasOnibusComponent},
{path:'linhasdelotacao',component:LinhasLotacaoComponent},
{path:'inicio', component:InicioComponent},
{path:'itinerario', component:ItinerarioComponent},
{path:'', component:InicioComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
