import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {LotacaoService} from '../lotacao.service';


@Component({
  selector: 'app-linhas-lotacao',
  templateUrl: './linhas-lotacao.component.html',
  styleUrls: ['./linhas-lotacao.component.css']
  })
  export class LinhasLotacaoComponent implements OnInit {

    linhas: Array<any> | undefined;
    searchText: string="";

    constructor(private lotacaoService: LotacaoService, private router:Router) { }

    ngOnInit() {
      this.listar();
    }

    listar(){
      this.lotacaoService.listar().subscribe (dados => this.linhas = dados);
    }

    teste(){this.router.navigateByUrl('/itinerario')}
  }
