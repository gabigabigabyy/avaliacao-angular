import { LinhasOnibusComponent } from './linhas-onibus/linhas-onibus.component';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OnibusService} from "./onibus.service";
import { HeaderComponent } from './header/header.component';
import { LinhasLotacaoComponent } from './linhas-lotacao/linhas-lotacao.component';
import { InicioComponent } from './inicio/inicio.component';
import { ItinerarioComponent } from './itinerario/itinerario.component';



@NgModule({
  declarations: [
    AppComponent,
    LinhasOnibusComponent,
    HeaderComponent,
    LinhasLotacaoComponent,
    InicioComponent,
    ItinerarioComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    Ng2SearchPipeModule
  ],
  providers: [OnibusService],
  bootstrap: [AppComponent]
})
export class AppModule { }
