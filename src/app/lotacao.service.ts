import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LotacaoService {

  linhasURL = 'http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%25&t=l'

  constructor(private http: HttpClient) { }

    listar(){
      return this.http.get<any[]>(`${this.linhasURL}`);
    }
   }
